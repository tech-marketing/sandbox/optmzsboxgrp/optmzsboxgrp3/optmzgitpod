# add code to chart a line chart with these value - 1,2,3,4,5,6 as X and a,b,c,d,e as Y
import matplotlib.pyplot as plt

x = [1, 2, 3, 4, 5, 6]
y = ['a', 'b', 'c', 'd', 'e', 'f']

plt.figure(figsize=(10, 6))
plt.plot(x, y, marker='o')
plt.xlabel('X-axis')
plt.ylabel('Y-axis')
plt.title('Line Chart')
plt.grid(True)
plt.show()
